\documentclass[10pt,a4paper]{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\graphicspath{{images/}}
\usepackage{parskip}
\usepackage{fancyhdr}
\usepackage{vmargin}
\setmarginsrb{3 cm}{2.5 cm}{3 cm}{2.5 cm}{1 cm}{1.5 cm}{1 cm}{1.5 cm}

\title{Rapport de Séminaire}                             % Title
\author{Maxime De Wolf}                               % Author
\date{\today}                                           % Date

\makeatletter
\let\thetitle\@title
\let\theauthor\@author
\let\thedate\@date
\makeatother

\pagestyle{fancy}
\fancyhf{}
\rhead{\theauthor}
\lhead{\thetitle}
\cfoot{\thepage}

\begin{document}
   	
   	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   	
   	\begin{titlepage}
   		\centering
   		\vspace*{0.5 cm}
   		\includegraphics[scale = 0.75]{UMONS}\\[1.0 cm]   % University Logo
   		\textsc{\LARGE Université de Mons}\\[2.0 cm]   % University Name
   		\textsc{\large Séminaire d'intelligence artificielle
   		}\\[0.5 cm]               % Course Name
   		\rule{\linewidth}{0.2 mm} \\[0.4 cm]
   		{ \huge \bfseries \thetitle}\\
   		\rule{\linewidth}{0.2 mm} \\[1.5 cm]
   		
   		\begin{minipage}{0.4\textwidth}
   			\begin{flushleft} \large
   				\emph{Auteur:}\\
   				\theauthor
   			\end{flushleft}
   		\end{minipage}~
   		\begin{minipage}{0.4\textwidth}
   			\begin{flushright} \large
   				\emph{Matricule: 151085}                                  % Your Student Number
   			\end{flushright}
   		\end{minipage}\\[2 cm]
   		
   		{\large \thedate}\\[2 cm]
   		
   		\vfill
   		
   	\end{titlepage}
   	
   	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   	
   	\tableofcontents
   	\pagebreak
   	
   	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   	
   	\section{Renaissance, promesses et limites de   		l’intelligence artificielle}
   	
   	L'intelligence artificielle, ou \textbf{IA}, n'est pas aussi jeune que l'on pourrait le croire. En effet, l'\textbf{IA} est étudiée depuis plus de 60 ans mais ce n'est que relativement récemment (entre 1975 et 1985) qu'elle a été remise au goût du jour.
   	
   	Depuis sa création, l'intelligence artificielle a eu beaucoup d'impacts sur notre société ne serait-ce que pour l'automatisation de tâches longtemps réservées à l'homme comme la conduite de véhicules ou la reconnaissance d'images.
   	
   	Pour classifier les différents types d'\textbf{IA}, nous les regroupons selon les fonctions cognitives qu'elles exercent. Ces fonctions cognitives sont au nombre de 5:\\
   	
   	\begin{enumerate}
   		\item \textbf{Les fonctions réceptives}: elles autorisent l’acquisition, le 
   		traitement, la classification et l’intégration de l’information. Ex: Diagnostic des mélanomes
   		
   		\item La \textbf{mémoire} et l’\textbf{apprentissage}	permettant le stockage et le rappel de l’information. Ex: \textit{AlphaGo} et \textit{AlphaGo Zero}
   		
   		\item Le \textbf{raisonnement},	la \textbf{pensée}.	Cela concerne aussi l’organisation et la réorganisation mentale de l’information ainsi que son utilisation. Ex: Démonstration automatique de théorèmes
   		
   		\item Les \textbf{fonctions expressives} qui rendent possible la communication. Ex: \textit{Chatbots}
   		
   		\item Les \textbf{fonctions exécutives} de prise de décisions et d’actions. Ex: Organes artificiels\\
   	\end{enumerate} 
   
	Il va s'en dire que l'avancée technologique obtenue grâce à l'émergence de l'intelligence artificielle bouleverse certaines notions de notre société. En effet, l'IA a permis une avancée tellement rapide que nous devons faire face à la re-conceptualisation de certains aspects de notre société. Ce phénomène est appelé \textbf{re-ontologisation}.
	
	Entre-autre, cela nous a fait entrer dans l'ère de l'\textit{industrie 4.0} où l'intelligence artificielle tient une place très importante notamment grâce aux objets connectés (\textit{IoT}), aux capteurs et à la réalité augmentée. En effet, ces trois technologies produisent une quantité énorme de données qu'il serait impossible d'analyser sans intelligence artificielle. Cela est notamment utile pour la gestion des stocks en temps réel (inventaire, commandes, ...) mais l'utilité de l'\textit{IA} est loin de se résumer à cette seule application.
	
	\begin{center}
		\includegraphics[width=0.5\linewidth]{sem1/industry_40}
	\end{center}
	
	Dans un autre contexte, l'intelligence artificielle est également très utilisée dans le domaine de la finance. En effet, elle permet entre autre aux banques de déterminer les risques et de choisir la meilleure action en connaissance de cause. Elle permet également aux assurances d'établir les primes suivant les informations propres à leurs clients.\\
	
	\newpage
	
	Outre toutes ces belles avancées, l'intelligence artificielle soulève également plusieurs interrogations parmi lesquelles la question de la vie privée. Effectivement, l'\textbf{IA} permet d'analyser une énorme quantité de données dont toutes celles relatives à nos moindres faits et gestes. Par exemple, nos habitudes de consommation sont analysées sur Internet, les objets connectés récoltent indirectement toutes sortes de données personnelles (heure de départ/retour au domicile, ...). Dès lors, nous sommes en droit de nous inquiéter sur le respect de notre vie privée remise en cause par l'analyse de toutes ces données.
	
	\begin{center}
		\includegraphics[width=0.5\linewidth]{sem1/IoT}
	\end{center}
	
	Un autre problème que cela suscite est que la collecte massive de données donne beaucoup de pouvoir aux entreprises qui les analysent. En effet, munis de ces données, les géants de l'information comme \textit{Facebook} et \textit{Google} peuvent en savoir long sur nos habitudes et se servir de ces informations dans leurs propres intérêts.\\
	
	Vous l'aurez compris, l'intelligence artificiel est un outil permettant d'énormes progrès technologiques mais cela suscite également beaucoup de questions d'ordre éthique. Il faut donc contrôler l'usage de l'\textbf{IA} pour éviter que l'on puisse s'en servir de manière abusive et empêcher celle-ci de devenir un véritable danger.
	
	\newpage
	
   	
   	\section{Comment mieux prédire les épidémies et les famines ?}
   	
   	La propagation d'une épidémie est un processus difficile à modéliser. En effet, la propagation ne se fait pas seulement de proche en proche mais est aussi grandement influencée par la mobilité humaine. Ce dernier point complexifie donc la modélisation de la propagation d'une épidémie. Il existe néanmoins deux approches afin d'y parvenir.\\
   	
   	La première approche consiste à modéliser le risque d'exporter une maladie d'une région \textit{A} vers une région \textit{B} grâce à la formule ci-dessous:
   	
   	\[
   		RisqueExport_{A \rightarrow B} = \beta \sum_{C=1}^{R} \alpha_{BC} N_B \frac{\alpha_{AC} I_A}{\sum_{k=1}^{R} \alpha_{KC} N_k }
   	\]
   	
   	La signification de toutes ces variables est décrite ci-dessous:\\
   	\begin{itemize}
   		\item $N_k$ : la population totale résidant dans la région $k$
   		\item $I_k$ : le nombre de résidents de la région $k$ infectés par la maladie
   		\item R : le nombre total de régions
   		\item $\beta$ : le taux de transmission de la maladie
   		\item $\alpha_{ij}$ :le pourcentage du temps que les résidents de la région $i$ passent dans la région $j$\\
   	\end{itemize}
   
   Bien que cette méthode fonctionne relativement bien, elle présente également certains inconvénients. En effet, cette formule demande l'accès à un certain nombre de données exposées ci-dessus. Ces données ne sont pas toujours faciles d'accès ni toujours fiables. De plus, la variable "$\alpha_{ij}$" ne peut être récupérée dans aucune base de données.
   
   Heureusement, nous pouvons déduire ces données grâce aux \textit{Call Detail Records} (\textbf{CDRs}) qui sont les registres des appels téléphoniques récoltés par les compagnies de téléphonie. Ces registres conservent pour chaque communication l'identification des deux utilisateurs que nous pouvons localiser grâce aux antennes qui ont relayés cette communication. L'utilisation de l'équation mentionnée ci-dessus est donc possible.
   
   \begin{center}
   		\includegraphics[width=0.75\linewidth]{sem2/CDR}
   \end{center}
   
   La modélisation de la propagation d'une maladie permet ensuite de construire des \textit{dashboards} pour la visualiser. Ils permettent ensuite de définir les régions à aider en priorité afin d'endiguer la propagation de l'épidémie.
   
   La seconde approche consiste à utiliser des modèles prédictifs faisant appel à de l'intelligence artificielle pour mener à l'élaboration d'autres \textit{dashboards}.
   
   \newpage
   
   L'intelligence artificielle (\textbf{IA}) est également utilisée dans le domaine de la santé pour prédire et ainsi pouvoir prévenir les crises alimentaires. La aussi, il existe deux méthodes afin d'y parvenir.
   
   La première consiste à se baser sur les données téléphoniques. Effectivement, il existe une corrélation entre les dépenses alimentaires et en téléphonie. Une anomalie dans la consommation téléphonique indique donc un éventuel début de crise alimentaire. Ces données de téléphonie sont donc analysées par une intelligence artificielle afin de détecter les crises alimentaires.
   
   La seconde méthode consiste à analyser les images satellites des zones à surveiller. Cette analyse est menée a bien par une intelligence artificielle qui va en déduire l'état des récoltes de différents types. Suite à cette analyse, nous pouvons en déduire s'il y a des risques de crise alimentaire ou pas. Pour ce faire, l'\textbf{IA} analyse les images satellites en utilisant la longueur d'onde des couleurs pour déterminer le type de culture et le pourcentage que les plantations occupent par rapport à la terre dans un champs.
   
   \begin{center}
   	\includegraphics[width=0.5\linewidth]{sem2/satellite}
   \end{center}
   
   Cette technique a cependant l'inconvénient de nécessiter une analyse manuelle afin d'entraîner l'intelligence artificielle. Cette phase demande beaucoup de temps car il faut évaluer manuellement le pourcentage de plantation dans un champs. Une fois cette phase effectuée, l'\textbf{IA} peut s'entraîner à faire des prévisions sur les régions analysées manuellement.\\
   
   En conclusion, l’intelligence artificielle a un rôle important à jouer dans les domaines de la santé et du développement. Cependant, l'\textbf{IA} n'est pas une technique adaptée à tous les cas de figure. En effet, l'\textbf{IA} a besoin d'un nombre relativement grand de données pour s'entraîner. Son utilisation dépend également du budget et du temps que l'on dispose mais aussi de la nécessité de pouvoir expliquer les décisions car l'intelligence fonctionne comme une boîte noire.
   
   
   
   \newpage

   \section{L’intelligence artificielle et le droit : vertiges d’un nouveau monde}
   
   L'évolution soudaine de l'intelligence artificielle (\textbf{IA}) demande une réflexion profonde sur la législation en vigueur. Effectivement, les changements que l'\textbf{IA} apporte sont si importants qu'ils demandent un changement de paradigme de notre système législatif.
   
   En effet, avec l'émergence de l'intelligence, la législation ne suffit plus à répondre à de nouvelles questions législatives qui sont apparues avec l'\textbf{IA}. Par exemple, si une voiture autonome renverse un piéton, qui est le responsable ? La même question se pose si un problème survient lors d'une livraison par drone. Les exemples de cette problématique sont multiples, il faut donc adapter la législation pour prendre en compte les avancées de l'intelligence artificielle.
   
    Cette problématique est assez complexe à résoudre car l'\textbf{IA} intervient dans divers domaines dont les suivants:
    \begin{itemize}
    	\item négociation de contrats (\textit{trading} en bourse, assurance, ...)
    	\item prestation de services (livraison, conduite de véhicule, ...)
    	\item collecte de données (déduction de l'âge, ...)
    	\item invention et œuvre (composition de partition de musique, ...)
    \end{itemize}
	De plus, l'Europe à une mentalité juridique différente des autres états comme les Etats-Unis qui légifèrent au fur et à mesure que les problèmes apparaissent alors que l'Europe essaye de prévoir tous les cas de figure avant que ceux-ci ne se produisent. Nous ne pouvons donc pas nous inspirer de ce qui a été fait ailleurs. 

	Heureusement le parlement essaie de résoudre cette problématique en répondant aux questions suivantes:
	\begin{itemize}
		\item Doit-on donner un statut juridique à l'intelligence artificielle ?
		\item  En cas de problème, qui est responsable ?
		\item  Comment protéger la vie privée vis-à-vis de l'\textbf{IA} ?
		\item A qui reviennent les droits de ce que l'\textbf{IA} a produit ?
	\end{itemize}
	Il existe déjà quelques pistes de réponses à ces questions.\\
	
	Pour ce qui est de la question du statut juridique, deux approches différentes peuvent être suivies: utiliser un statut qui existait autre fois ou en créer un nouveau.
	
	La première est de donner à l'intelligence artificielle un statut d'esclave de droit romain. C'est l'option choisie par les \textbf{USA}, ce statut juridique était déjà présent dans leur législation. Ce statut permet de décharger la responsabilité de l'\textbf{IA} sur son "maître". Cela permet entre-autre, lors d'une négociation de contrat, de le négocier au nom du maître et non au nom de l\textbf{IA}.
	
	L'approche utilisée par l'Europe est différente car elle a décidé de créer un tout nouveau statut juridique pour l'intelligence artificiel: le statut de robot. Ce statut rend le robot responsable de ses actions.\\
	
	L'intelligence artificielle cause également des soucis au niveau du respect de la vie privée. En effet, une \textbf{IA} collecte énormément d'informations qui sont ensuite stockées dans des bases de données. Le problème c'est que son propriétaire peut utiliser ces données pour un but tout à fait différent. 
	
	Heureusement, le parlement Européen a proposé une piste de solution: le \textbf{RGPD}. Pour résumer, le \textbf{RGPD} donne cinq règles à respecter:
	\begin{itemize}
		\item Transparence: le fournisseur de service doit informer le client des informations qu'il collecte ainsi que la raison pour laquelle ces données sont utilisées
		\item Égalité: tous les utilisateurs doivent être traités de la même manière
		\item Limitation aux finalités annoncées: Les données collectées ne peuvent être utilisées que pour les finalités annoncées à l'utilisateur
		\item Minimisation des données: le fournisseur de service ne peut collecter que les données utiles au service qu'il fournit
		\item Profilage et décisions automatisées: le client doit être mis au courant s'il est soumis à des décisions entièrement automatisées. Le cas échant, il a également le droit de demander que la décisions soit réexaminée par un être humain
	\end{itemize}

	Malheureusement, il existe des failles dans le \textbf{RGPD} qui permettent de le contourner.\\
	
	Pour résoudre la question des droits d'auteur, il faut d'abord se demander s'il existe des droits d'auteur sur une œuvre créée par une intelligence artificielle. Si c'est effectivement le cas, il faut décider à qui reviennent les droits: au domaine public, au concepteur, à l'\textbf{IA} elle-même, au propriétaire de l'\textbf{IA} ou au propriétaire de la machine qui fait tourner l'\textbf{IA}.
	
	Cette question est épineuse car elle définit qui gagnera de l'argent grâce aux créations de l'intelligence artificielle.\\

	Malheureusement, l'intelligence artificiel est un domaine que le parlement ne connaît pas bien. Cela implique qu'il essaie de comprendre l'\textbf{IA} à travers la culture populaire. A cause de cela, le parlement à une vision assez négative de l'\textbf{IA}.\\
	
	En conclusion, il est très important et relativement urgent de modifier la législation pour prendre en compte l'influence de l'intelligence artificielle sur notre société. Cependant, cela est un processus lent et complexe. Il faut donc y réfléchir dès maintenant et comprendre suffisamment le sujet pour être sûr de prendre les bonnes décisions.
   
   \newpage	

   \section{La créativité est-elle soluble dans la technologie?}
   	
   Il existe deux types de connaissance, la \textit{Street Knowledge} et la \textit{Book Knowledge}.
   
   La première est la connaissance par l'expérience ou connaissance implicite. Elle représente le savoir-faire, c'est-à-dire toutes les capacités acquises par nous-même comme la reconnaissance d'images ou de sons, la capacité de marcher, la capacité de parler, ...
   
   La deuxième est la connaissance par le langage, elle représente donc le savoir. Elle représente les connaissances que nous enseignent les autres comme les mathématiques, les sciences, la médecine, ... De nos jours, ces connaissances sont faciles d'accès grâce à Internet.\\
   
   Avant, nous essayions de créer des intelligences artificielles version \textit{Book Knowledge}. Pour réaliser cela, nous avons créé les systèmes experts que nous avons cherché à rendre intelligents en leur explicitant les connaissances grâce à des langages informatiques. Un de ces systèmes experts les plus connus est \textbf{Deep Blue} d'\textbf{IBM} qui a réussi à battre le champion du monde d'échecs en 1997.
   
   Maintenant nous créons de l'intelligence artificielle basée sur les \textit{Street Knowledge}. Cela nous permet d'apprendre à la machine à faire des choses qu'on ne sait pas expliquer -et donc programmer- comme la reconnaissance d'images. Cependant, ce type d'\textbf{IA} agit comme une boite noire car nous n'avons aucun moyen de connaître son raisonnement pour arriver au résultat qu'elle obtient.
   
   Le prochain défi de l'intelligence artificielle est donc de verbaliser les connaissances acquises par la machine. Cette nouvelle sorte d'intelligence artificielle est appelée \textit{eXplainable Artificial Intelligence} (\textbf{XAI}). En plus de fournir le résultat de ce pour quoi elle a été entraînée, la \textbf{XAI} fournit également l'explication de son "raisonnement" pour y parvenir.\\
   
   \begin{center}
   		\includegraphics[width=0.75\linewidth]{sem4/XAI}
   \end{center}
   
   Il existe trois grandes étapes pendant un processus créatif:
   \begin{enumerate}
   	\item La création
   	\item L'interprétation
   	\item La diffusion
   \end{enumerate}
	Chacune de ces étapes peut être assistée ou automatisée par ordinateur.
	
	L'étape de création peut-être assistée ou automatisée grâce à l'intelligence artificielle. Effectivement, il existe des \textbf{IA} capables de créer -par exemple- de la musique. On peut par la suite s'en servir comme base pour l'améliorer, il s'agit alors de création assistée. Sinon, on peut également considérer ces musiques comme produit fini, il s'agit alors de création automatisée.
	
	Il est également possible d'assister voir même d'automatiser l'étape d'interprétation. Par exemple, on voit de plus en plus de concerts donnés par des avatars holographiques. Il s'agit donc d'interprétation assistée.
	Il existe aussi des robots \textit{jazzman} contrôlés par une intelligence artificielle qui leur permet d'improviser et donc de se comporter comme les autres musiciens. On parle alors d'interprétation assistée.
	
	Enfin, pour l'étape de diffusion, il existe des intelligences artificielles capables d'assister la régie son et lumières en gérant certains paramètres pour nous. Il y a également des caméras intelligentes capables de filmer les points d'intérêts et de monter automatiquement la vidéo. Elles sont entre-autre utilisées dans les studios de radio pour la diffusion télévisée. Ces caméras permettent donc d'automatiser ce processus.\\
	
	Il a été scientifiquement prouvé que les émotions sont étroitement liées à notre corps. Dès lors, on peut supposer qu'une intelligence artificielle n'a pas la capacité de ressentir les émotions. Dès lors nous sommes en droit de nous poser cette question: comme une \textbf{IA} ne peut ressentir des émotions, serait-elle capable d'en percevoir chez nous et d'en exprimer.
	
	Pour répondre partiellement à cette question, nous pouvons faire l'analogie avec la ressemblance entre un avion et un oiseau. En effet, l'avion ne sait pas battre des ailes à l'inverse de l'oiseau mais ils savent voler tous les deux. L'avion n'imite pas le comportement de l'oiseau mais s'en inspire pour arriver à voler comme lui. On peut donc partir du principe qu'une \textbf{IA} peut s'inspirer de notre capacité à ressentir les émotions pour pouvoir les percevoir et les exprimer à son tour sans toutefois être capable de les ressentir.
   
   \newpage

   \section{AI within the energy transition: how can Machine Learning help the Electric Power System ?}
   
   L'énergie électrique est très difficile à stocker. Il est donc vital pour les fournisseurs d'électricité d'accorder en temps réel leur production en fonction des besoins des clients car une production trop élevée signifie un gaspillage d'énergie et donc une perte d'argent tandis qu'une production trop basse signifie un manque à gagner.\\
   
   Cependant, le marché de l'électricité s'est complexifié au fil des ans. En effet, avant il n'y avait que les grands producteurs qui fournissaient de l'électricité.
   
   Maintenant, il y a le marché des énergies renouvelables qui occupe une plus grande part de marché et donc on ne contrôle pas la production. Il y a également des producteurs tierces comme les clients qui possèdent -par exemple- des panneaux photo-voltaïque qui injectent leur électricité sur le réseau.
   Le mode de consommation a aussi changé car il existe plusieurs sortes de contrats: contrat à l'année, contrat au jour, ...
   
   Tous ces facteurs ont fait passer le marché de l'électricité d'un état relativement statique à un marché beaucoup plus flexible. La prévision de la demande d'électricité est donc devenu un processus très complexe. Pour cette raison, les producteurs d'électricité se tournent vers l'intelligence artificielle pour résoudre ce problème.\\
   
   \begin{center}
   		\includegraphics[width=0.75\linewidth]{sem5/EDF}
   \end{center}
   
   Une intelligence artificielle a besoin d'énormément de données pour pouvoir s'entraîner. Effectivement, peu importe le type d'apprentissage utilisé, plus le jeu de données d'entraînement est grand et plus les prédictions de l'intelligence artificielle auront tendance à être justes.
   
   Heureusement, les compagnies d'électricité ont toujours récolté des données de consommation. Le problème c'est que ces bases de données ne sont souvent pas complètes à cause de soucis techniques comme par exemple, un capteur qui tombe en panne et qui ne peut donc plus récupérer de donnée. Malheureusement, ces bases de données imparfaites posent problème lors de l'entraînement de l'intelligence artificielle. La solution la plus simple est de ne pas utiliser les entrées dont une donnée est manquante. Cette solution peut réduire significativement le nombre d'entrées utiles et donc impacter l'entraînement de l'\textbf{IA}.
   
   \newpage 
   
   Cependant, il existe des techniques plus avancées pour éviter cela. Effectivement, il est possible de compléter les entrées incomplètes en se basant sur des entées similaires grâce à des techniques de système de recommandation. Grâce à ce procédé, on élimine les imperfections de la base de données sans perte de données. L'entraînement de l'intelligence artificielle qui s'en suit n'est donc pas impacté.\\
   
   En conclusion, la prévision de la consommation électrique est un élément complexe à réaliser et crucial pour les fournisseurs d'électricité. Beaucoup de techniques différentes sont utilisées pour y parvenir car c'est un domaine rempli de challenges qu'il faut pouvoir surmonter pour se démarquer de son concurrent. On peut donc dire que c'est un domaine en pleine innovation car le marché de l'électricité est en pleine transition. 
   
   \newpage	   		

   \section{Pour une Intelligence artificielle responsable}
   
   Cela fait maintenant plus de 50 ans que l'informatique se développe, devenant toujours de plus en plus puissant. Cette montée en puissance a mené à des percées fulgurantes depuis 2010 tel que la \textit{Google car}, la voiture autonome lancée par \textit{Google} ou la victoire d'une intelligence artificielle contre le champion du monde de \textit{Go}. Maintenant, nous sommes même capables de construire des neurones artificiels plus performants que les nôtres.
   
   Face à de tels avancées, les autorités cherchent à encadrer l'intelligence artificielle pour que celle-ci ait un développement responsable. Cela se traduit par différents principes à appliquer décidés lors de la déclaration de Montréal. Cette déclaration poursuit plusieurs objectifs:
   
   \begin{enumerate}
   	\item Élaborer un cadre éthique pour le développement et le déploiement de l’IA
   	\item Orienter la transition numérique afin que tous puissent bénéficier de cette révolution technologique
   	\item Ouvrir un espace de dialogue national et international pour réussir collectivement un développement inclusif, équitable et écologiquement soutenable de l’IA
   \end{enumerate}

	Cette déclaration de Montréal traduit plusieurs préoccupations de notre société vis-à-vis de l'intelligence artificielle. Parmi celles-ci on retrouve:
	
	\begin{itemize}
		\item Est-il possible de perdre le contrôle de nos outils technologiques?
		\item Même si nous pouvons le faire, devons nous ?
		\item Aurons-nous la prévoyance de concevoir des garde-fous pour nous protéger contre les conséquences imprévues?
		\item La troisième loi d’\textit{Arthur C. Clarke}: "Toute technologie suffisamment avancée est indiscernable de la magie"
	\end{itemize}

	Cette convention s'articule autour de 10 principes:
	
	\begin{enumerate}
		\item Bien-être
		\item Respect de l’autonomie
		\item \textbf{Protection intimité et vie privée}
		\item Solidarité
		\item Participation démocratique
		\item Équité
		\item Inclusion de la diversité
		\item \textbf{Prudence}
		\item \textbf{Responsabilité} 
		\item Développement soutenable
	\end{enumerate}

	Principe de \textbf{Responsabilité}: il faut responsabiliser tous les grands acteurs de l'intelligence artificielle afin d'établir des règles communes que tous acceptent de suivre pour mettre en place une utilisation éthique de l'\textbf{IA}. Effectivement, l'éthique ne passe pas forcément par les lois et elles changent d'un pays à l'autre. Ce principe de responsabilité est donc très important.
	
	\newpage
	
	Principe de \textbf{Protection intimité et vie privée}: de nos jours, il est quasiment impossible de rester anonyme. En effet, il y a de fortes chances que votre empreinte numérique soit unique, ce qui veut dire qu'elle peut vous servir d'identifiant à travers tout l'Internet. Cela veut dire qu'anonymiser les données ne sert à rien car il sera toujours possible de vous identifier grâce à votre empreinte. Nos informations peuvent donc être utilisées même en prenant des précautions, il faut donc conscientiser les grands acteurs de l'\textbf{IA} pour qu'ils ne le fassent pas.
	
	Principe de \textbf{Prudence}: il ne faut pas faire une confiance aveugle à l'intelligence artificielle. Effectivement, il a été démontré que l'\textbf{IA} n'était pas très robuste, notamment via les \textit{One Pixel Attack}.\\
	
	\begin{center}
		\includegraphics[width=0.75\linewidth]{sem6/OPA}
	\end{center}
	
	Pour conclure, l'intelligence artificielle est un outil très puissant mais elle peut également causer beaucoup de torts si elle est exploitée à mauvais escient. C'est pourquoi nous devons conscientiser les grands acteurs de l'intelligence artificielle pour qu'ils l'utilisent de manière éthique pour garantir le bien-être commun.
   
   \newpage
      	
   \section{Évaluations personnelles des conférences}
   
   Cette section présente une évaluation personnelle de chaque conférence. Vous y trouverez donc notre avis ainsi qu'une petite liste des points que nous avons aimé et des points que nous n'avons pas aimé.
   
   \subsection*{Renaissance, promesses et limites de   		l’intelligence artificielle}
   
   Nous avons aimé cette conférence car elle sort du cadre technique que nous avons l'habitude de voir en cours. L'orateur a également dressé une ligne du temps représentant l'évolution de l'intelligence artificielle ainsi qu'un rappel des dernières avancées que cette technologie a rendu possible.
   
   Cela nous a permis de nous rendre compte de l'\textbf{IA} évoluait de plus en plus vite et donc que son influence suit la même croissance. Nous avons donc compris l'importance de réfléchir aux conséquences de l'utilisation de l'intelligence artificielle.
   
   \subsection*{Comment mieux prédire les épidémies et les famines ?}
   
   Cette conférence est intéressante car elle permet de voir à quel point l'intelligence artificielle a un rôle important à jouer dans la préventions des épidémies et des famines.
   
   L'orateur a également parlé du côté technique pour expliquer comment leur \textbf{IA} fonctionne. Il est rentré juste assez dans les détails pour comprendre sur quels principes leur technique se base sans que cela soit difficile à comprendre. C'est donc un bon point.
   
   \subsection*{L’intelligence artificielle et le droit : vertiges d’un nouveau monde}
   
   Nous avons trouvé cette conférence très instructive car elle aborde un domaine que nous ne connaissons que très peu, le droit. En effet, nous avons appris que le système avait besoin de s'adapter face aux progrès de l'intelligence artificielle. Il est aussi intéressant de noter que certains intéressés tentent d'influencer ce changement juridique en leur faveur. En outre, nous avons pris conscience grâce à cette conférence que la manière dont le parlement change la loi sera décisif pour ceux qui utilisent l'\textbf{\textit{IA}} de manière commerciale.
   
   \subsection*{La créativité est-elle soluble dans la technologie?}
   
   Tout d'abord, nous avons aimé la première partie de cette conférence qui consiste en un "mini-concert" donné par l’\textit{Orchestre Royal de Chambre de Wallonie}.
   
   Nous avons également apprécié la deuxième partie de cette conférence durant laquelle nous avons découvert quelle place l'intelligence artificielle occupe dans le domaine de l'art. Il y a également eu une petite réflexion philosophique en se demandant "Est-ce qu'une intelligence artificielle est capable de ressentir des émotions ?".
   
   \subsection*{AI within the energy transition: how can Machine Learning help the Electric Power System ?}
   
   Cette conférence nous a permis de connaître les challenges qu'il faut surmonter dans le secteur du marché de l'électricité ainsi que son évolution au fil des années. Cependant, nous pensons que l'orateur s'est trop focalisé sur les détails techniques qui n'étaient pas nécessaires à la compréhension globale de sa présentation. Cette conférence est donc plus difficile à suivre que les autres selon nous.
   
   \subsection*{Pour une Intelligence artificielle responsable}
   		
   	Cette conférence expose plus ou moins le même sujet que la troisième. Heureusement, ces deux orateurs ne l'ont pas abordé de la même façon. Cela a permis d'avoir un autre point de vue sur ce sujet et ainsi d'en avoir une meilleure compréhension. Cet orateur a choisi de mettre l'accent sur tous les abus qu'on pouvait faire en se servant de l'\textbf{IA} pour montrer qu'il faut être très prudent quand on s'en sert et la nécessité d'encadrer son utilisation pour les éviter. Cette conférence est donc très intéressante.
   		
   \newpage

          	
\end{document}